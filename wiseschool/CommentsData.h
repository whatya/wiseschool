//
//  CommentsData.h
//  WiseSchool
//
//  Created by EnvisionMobile on 15/7/18.
//  Copyright (c) 2015年 whatya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentsData : NSObject
@property(nonatomic,retain) NSString *commentsHead;
@property(nonatomic,retain) NSMutableArray *commentArray;
@end
