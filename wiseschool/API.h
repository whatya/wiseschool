//
//  API.h
//  wiseschool
//
//  Created by 张宝 on 15/7/24.
//  Copyright (c) 2015年 whatya. All rights reserved.
//

#ifndef wiseschool_API_h
#define wiseschool_API_h

#define HTTP_SUCCEED_FLAG @"1"

#define API_NAME_LOGIN_VALIDATE_MOBILE @"zhxy_v3_java/app/login/validateMobile.app"

#define API_NAME_LOGIN_GET_AREA_FOR_CITY @"zhxy_v3_java/app/common/areaInfo.app"

#define API_NAME_LOGIN_GET_SCHOOL_FOR_AREA @"zhxy_v3_java/app/common/schoolInfo.app"

#define API_NAME_LOGIN_GET_GRADE_FOR_SCHOOL @"zhxy_v3_java/app/common/gradeInfo.app"

#define API_NAME_LOGIN_GET_SUBJECT_INFO @"zhxy_v3_java/app/common/subjectInfo.app"

#define API_NAME_CLASS_GET_COURSE_INFO @"zhxy_v3_java/app/course/courseInfo.app"//获取课程表

#define API_NAME_CLASS_CREATE_COURSE @"zhxy_v3_java/app/common/subjectCreate.app" //添加科目信息

#define API_NAME_CLASS_SET_COURSE @"zhxy_v3_java/app/course/courseMerge.app"//设置课程表

#endif
