//
//  CommentDTO.h
//  WiseSchool
//
//  Created by EnvisionMobile on 15/7/4.
//  Copyright (c) 2015年 whatya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentDTO : NSObject

@property(nonatomic,retain) NSString *iconUrl;
@property(nonatomic,retain) NSString *content;
@end
