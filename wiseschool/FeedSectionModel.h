//
//  FeedSectionModel.h
//  WiseSchool
//
//  Created by 张宝 on 15/7/10.
//  Copyright (c) 2015年 whatya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeedSectionModel : NSObject
@property (nonatomic,strong) NSString *sectionTitle;
@property (nonatomic,strong) NSArray *feedsList;
@end
